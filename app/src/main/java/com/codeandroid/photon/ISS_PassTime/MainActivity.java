package com.codeandroid.photon.ISS_PassTime;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.codeandroid.photon.ISS_PassTime.Model.Dagger.MyApplicationDaggerBuild;
import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyEvent;
import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyFailureEvent;
import com.codeandroid.photon.ISS_PassTime.Utils.LocationUtils.DeviceLocation;
import com.codeandroid.photon.ISS_PassTime.Utils.ConnectionUtils.CheckNetwork;
import com.codeandroid.photon.ISS_PassTime.View.ISSPassTimeViewFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;


public class MainActivity extends AppCompatActivity {

    //Adding the Views of the Layout
    ISSPassTimeViewFragment mISSPassTimeViewFragment;
    TextView internetStatus;

    //Injecting the CheckNetwork provided by Dagger MyApplicationDaggerModule
    @Inject
    CheckNetwork checkNetwork;
    @Inject
    DeviceLocation deviceLocation;

    private boolean locationpermission = false;
    private boolean network_connection = false;
    private boolean location_enabled = false;
    private CoordinatorLayout coordinatorLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        internetStatus = findViewById(R.id.nointernet);
        coordinatorLayout = findViewById(R.id.main);
        //Injecting the Dagger Component so that the Dagger provided values can be used.
        MyApplicationDaggerBuild.getMyApplicationDaggerBuild().getMyApplicationDaggerComponent().inject(this);
        connectionCheck();
        onloadFragment();
    }

    /**
     * Check if device is connected to internet and location setting is enabled in device.
     */
    private void connectionCheck() {
        network_connection = checkNetwork.getNetworkConnectionStatus();
        location_enabled = deviceLocation.isLocationEnabled();

    }

    /**
     * if location is not enabled, show alert dialog and prompt user to enable location in the device.
     * if no connection to internet, show alert dialog and prompt user to change connection setting.
     * else show data in the recycler view.
     */
    private void onloadFragment() {
        if (!location_enabled) {
            showGPSNotEnabled("Enable Location", "Location is not enabled");
        }
        if (!network_connection) {
            showNoConnection("No internet", "Internet/wifi is not enabled");
        }
        if(location_enabled && network_connection){
            loadView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        connectionCheck();
        if (location_enabled && network_connection) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    internetStatus.setVisibility(View.GONE);
                    setMainFragment();
                }
            }
        }


    }

    /**
     * Check permission and if permission is available, request and show the response.
     */
    private void loadView() {
        permissionsCheck();
        if (locationpermission) {
            internetStatus.setVisibility(View.GONE);
            setMainFragment();
        } else {
            clearMainFragment();
            internetStatus.setVisibility(View.VISIBLE);
            internetStatus.setText(getResources().getString(R.string.internetstatus));
        }

    }

    /**
     * if user cancels permissions or disable connection or disable location clears fragment from the view.
     */
    private void clearMainFragment() {
        if (mISSPassTimeViewFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mISSPassTimeViewFragment).commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Attach fragment to the main view.
     */
    public void setMainFragment() {
        mISSPassTimeViewFragment = new ISSPassTimeViewFragment();
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainfragment, mISSPassTimeViewFragment);
        fragmentTransaction.commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Refreshing the UI with Updated Network Response
            case R.id.refresh:
                onloadFragment();
                return true;

            default:
                break;
        }

        return false;
    }

    /**
     * Checks FINE_LOCATION permission. If not available, ask user for permission.
     */
    private void permissionsCheck() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}
                        , 6);
            }

        } else {
            locationpermission = true;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults != null) {
            if (requestCode == 6 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Adding the ISSPassTimeViewFragment to Fragment Container to Display Response
                locationpermission = true;

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        internetStatus.setVisibility(View.VISIBLE);
                        internetStatus.setText(getResources().getString(R.string.permissionStatus));
                    } else {
                        showPermmissionAlert();

                    }
                }
            }
        }
    }

    /**
     * Show alert dialog if Location is not enabled.
     *
     * @param title   title of the alert dialog.
     * @param message message shows that location is not enabled in device.
     */
    private void showGPSNotEnabled(String title, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton(("Open Settings"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                //get gps
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Log.e("..", "Cancel button is clicked.");
                internetStatus.setText("Location must be enabled in a device to see ISS passtime");
                internetStatus.setVisibility(View.VISIBLE);

            }
        });
        dialog.show();
    }

    /**
     * Shows alert dialog if there is no internet connection.
     *
     * @param title   title of the alert dialog.
     * @param message message to explain that there is no internet connection.
     */
    private void showNoConnection(String title, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setPositiveButton(("Open Settings"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivity(myIntent);
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Log.e("..", "Cancel button is clicked.");
                internetStatus.setText("No Connection. Please check your internet.");
                internetStatus.setVisibility(View.VISIBLE);

            }
        });
        dialog.show();
    }


    /**
     * If user clicks 'Do not show this again' is alert dialog, a snack bar is shown to
     * show the importance of permission to proceed in the application.
     */
    private void showPermmissionAlert() {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Permission is important for ISS time passes.", Snackbar.LENGTH_INDEFINITE)
                .setAction("Allow Permission", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                });

        snackbar.show();
    }

    /**
     * This method is invoked when request is successfull.
     * @param openNotifyEvent Custom event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOpenNotifyEvent(OpenNotifyEvent openNotifyEvent) {
        // Passing the Network Response to UI
        mISSPassTimeViewFragment.onSuccessOpenNotifyApiResponse(openNotifyEvent.getListOfResponse());

    }

    /**
     *This method is invoked when request fails.
     * @param openNotifyFailureEvent Custom failure event.
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOpenNotifyFailureEvent(OpenNotifyFailureEvent openNotifyFailureEvent) {
        // Passing the Network Response to UI
        mISSPassTimeViewFragment.onFailureOpenNotifyApiResponse(openNotifyFailureEvent.getOnFailureResponse());

    }


}
