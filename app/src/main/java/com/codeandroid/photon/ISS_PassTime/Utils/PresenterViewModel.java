package com.codeandroid.photon.ISS_PassTime.Utils;

import android.arch.lifecycle.ViewModel;

import com.codeandroid.photon.ISS_PassTime.Presenter.MainPresenter;


public class PresenterViewModel extends ViewModel {
    private MainPresenter mainPresenter;

    public MainPresenter getMainPresenter(){
        if(mainPresenter!=null){
            return mainPresenter;
        }else {
           mainPresenter=new MainPresenter();
        }
        return mainPresenter;
    }

}
