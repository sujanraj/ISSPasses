package com.codeandroid.photon.ISS_PassTime.Model.Dagger;

import android.content.Context;

import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyData;
import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyDataParser;
import com.codeandroid.photon.ISS_PassTime.Utils.LocationUtils.DeviceLocation;
import com.codeandroid.photon.ISS_PassTime.Utils.ConnectionUtils.CheckNetwork;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Provides;
import retrofit2.Retrofit;

/**
 * MyApplicationDaggerModule is the  Dagger MyApplicationDaggerModule class
 * It contains all the Provide methods that will be Injected through Dagger Component
 */
@dagger.Module
public class MyApplicationDaggerModule {


    private final Context context;

    public MyApplicationDaggerModule(Context context) {
        this.context = context;
    }


    //    Method to get Context of the Application
    @Singleton
    @Provides
    Context provideContext() {
        return context;
    }

    //    Method to get Retrofit instance from the OpenNotifyData class
    @Singleton
    @Provides
    @Named("Real")
    Retrofit providesRetrofitAdapter() {
        return new OpenNotifyData().getRetrofitAdapter();
    }

    @Singleton
    @Provides
    @Named("Dummy")
    Retrofit provideRetrofitForTesting() {
        return new OpenNotifyData().getRetrofitForTesting();
    }


    @Singleton
    @Provides
    OpenNotifyDataParser providesOpenNotifyDataParser() {
        return new OpenNotifyDataParser();
    }

    //    Method to get the instance of the CheckNetwork class
    @Singleton
    @Provides
    CheckNetwork providesInternetCheck(Context context) {
        return new CheckNetwork(context);
    }

    //    Method to get the instance of the DeviceLocation class
    @Singleton
    @Provides
    DeviceLocation providesGPSDevice(Context context) {
        return new DeviceLocation(context);
    }


}
