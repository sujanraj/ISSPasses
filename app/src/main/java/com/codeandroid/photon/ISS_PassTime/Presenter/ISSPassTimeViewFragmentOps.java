package com.codeandroid.photon.ISS_PassTime.Presenter;

import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyPOJO.Response;

import java.util.List;

public interface ISSPassTimeViewFragmentOps {
    public void onSuccessOpenNotifyApiResponse(List<Response> responseList);

    public void onFailureOpenNotifyApiResponse(String onFailureResponse);
}
