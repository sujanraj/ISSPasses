package com.codeandroid.photon.ISS_PassTime.View;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyEvent;
import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyFailureEvent;
import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyPOJO.Response;
import com.codeandroid.photon.ISS_PassTime.Presenter.ISSPassTimeViewFragmentOps;
import com.codeandroid.photon.ISS_PassTime.Presenter.MainPresenter;
import com.codeandroid.photon.ISS_PassTime.R;
import com.codeandroid.photon.ISS_PassTime.Utils.PresenterViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;


public class ISSPassTimeViewFragment extends Fragment implements ISSPassTimeViewFragmentOps {

    private MainPresenter mainPresenter;
    private RecyclerView recyclerView;
    private ISSPassTimeRecyclerViewAdapter ISSPassTimeRecyclerViewAdapter;
    private Context context;
    private TextView status;
    private ProgressBar progressBar;
    private PresenterViewModel presenterViewModel;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    //  This Method will give presenter OnStart feature of the Fragment
    //  To Support te EventBus feature.


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mainfragment, container, false);
        recyclerView =  view.findViewById(R.id.datapresenter);
        status = view.findViewById(R.id.status);
        progressBar = view.findViewById(R.id.progressBar);
        presenterViewModel= ViewModelProviders.of(this).get(PresenterViewModel.class);
        mainPresenter = presenterViewModel.getMainPresenter();
        getDeviceLocation();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mainPresenter.onStart();
    }

    //This Method will give presenter OnStop feature of the Fragment
    //  To Support te EventBus feature.
    @Override
    public void onStop() {
        super.onStop();
        mainPresenter.onStop();
    }



    /**
     * Request device location and show International Space Station Pass Times
     * in recycler view.
     */
    private void getDeviceLocation() {
        mainPresenter.requestDeviceLocation();
            status.setText(getResources().getString(R.string.loadingmessage));
            recyclerView.setVisibility(View.GONE);
            status.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);

    }


    /**
     * Called when request to Open-Notify.org is a success and response is received.
     * Invoked by MainPresenter.
     * @param responseList response from Open-Notify.org api.
     */
    @Override
    public void onSuccessOpenNotifyApiResponse(List<Response> responseList) {
        Log.e("###", "size " + responseList.size());
        //Passing the Network Response List to RecyclerViewAdapter
        ISSPassTimeRecyclerViewAdapter = new ISSPassTimeRecyclerViewAdapter(responseList);
        //Setting the Layout to RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        // Setting the Adapter to RecyclerView
        recyclerView.setAdapter(ISSPassTimeRecyclerViewAdapter);
        status.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

    }

    /**
     * Called when request to Open-Notify.org is a success and response is received.
     * Invoked by MainPresenter.
     * @param onFailureResponse failed response from the api.
     */
    @Override
    public void onFailureOpenNotifyApiResponse(String onFailureResponse) {
        recyclerView.setVisibility(View.GONE);
        status.setText(onFailureResponse);
        status.setVisibility(View.VISIBLE);

    }


}
