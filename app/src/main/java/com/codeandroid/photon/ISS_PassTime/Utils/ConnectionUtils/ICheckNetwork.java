package com.codeandroid.photon.ISS_PassTime.Utils.ConnectionUtils;

/**
 * Created by macbookpro on 3/14/18.
 */

public interface ICheckNetwork {
    boolean getNetworkConnectionStatus();
}
