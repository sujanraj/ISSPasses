package com.codeandroid.photon.ISS_PassTime.Model.Dagger;

import com.codeandroid.photon.ISS_PassTime.MainActivity;
import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyDataParser;
import com.codeandroid.photon.ISS_PassTime.Presenter.MainPresenter;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {MyApplicationDaggerModule.class})
public interface MyApplicationDaggerComponent {

    void inject(MainPresenter mainFragment);

    void inject(MainActivity mainActivity);

    void inject(OpenNotifyDataParser openNotifyDataParser);

}
