package com.codeandroid.photon.ISS_PassTime.View;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.codeandroid.photon.ISS_PassTime.R;

/**
 * ISSPassTimeDataViewHolder is the RecyclerViewViewHolder.
 * It will contain the  views of Layout file used in RecyclerView.
 */

public class ISSPassTimeDataViewHolder extends RecyclerView.ViewHolder {
    TextView duration, time;

    public ISSPassTimeDataViewHolder(View itemView) {
        super(itemView);
        duration = itemView.findViewById(R.id.duration);
        time = itemView.findViewById(R.id.time);
    }
}
