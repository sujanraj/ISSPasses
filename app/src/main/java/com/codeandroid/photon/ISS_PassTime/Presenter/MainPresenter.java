package com.codeandroid.photon.ISS_PassTime.Presenter;

import android.location.Location;

import com.codeandroid.photon.ISS_PassTime.Model.Dagger.MyApplicationDaggerBuild;
import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyDataParser;
import com.codeandroid.photon.ISS_PassTime.Utils.LocationUtils.DeviceLocation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

/**
 * MainPresenter is the Presenter class that is responsible to communicate between UI and model classes .
 */

public class MainPresenter {

    //Injecting the Dagger values
    @Inject
    OpenNotifyDataParser openNotifyDataParser;
    @Inject
    DeviceLocation deviceLocation;


    public  MainPresenter(){
        // Injecting the Dagger Component so that the Dagger provided values can be used.
        MyApplicationDaggerBuild.getMyApplicationDaggerBuild().getMyApplicationDaggerComponent().inject(this);
    }

    // Registering for the EventBus Events
    public void onStart() {
        EventBus.getDefault().register(this);
    }

    // Removing the Registered Events
    public void onStop() {
        EventBus.getDefault().unregister(this);
    }



    /**
     * Called when user's location is received
     * Observes location event from DeviceLocation.java
     *
     * @param location location of the user.
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGPSDeviceEvent(Location location) {
        makeOpenNotifyAPIcall(location.getLatitude(), location.getLongitude());

    }


    /**
     * Called after receiving device's location.
     * @param lat latitute
     * @param lon longitude
     */
    public void makeOpenNotifyAPIcall(Double lat, Double lon) {
        openNotifyDataParser.getJSONDataFromAPI(lat, lon);
    }

    /**
     * request device's location
     */
    public void requestDeviceLocation() {
        deviceLocation.getDeviceLocation();

    }


}
