package com.codeandroid.photon.ISS_PassTime.Utils.LocationUtils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import org.greenrobot.eventbus.EventBus;

/**
 * DeviceLocation is used to get the Device latitude and Longitude using the GPS Provider or Network provider.
 */

public class DeviceLocation implements ICheckGPS, LocationListener{
    boolean gps_enabled = false;
    boolean network_enabled = false;
    private LocationManager locationManager;
    private Context context;
    private String locationListenerTag = "LOCATIONS";
    private String ProviderTag = "Provider";
    private String GPSProviderTag = "GPS_PROVIDER";
    private String NetworkProviderTag = "NETWORK_PROVIDER";
    private FusedLocationProviderClient mFusedLocationClient;


    public DeviceLocation(Context context) {
        super();
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

}

    /**
     * Request location of the device.
     */
    public void getDeviceLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (gps_enabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            } else if (network_enabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            } else {
                Toast.makeText(context, "Provider is not enabled. Please enable location in Settings", Toast.LENGTH_LONG).show();
                Log.e("###", "Failed");
            }

        }
    }

    /**
     * Invoked to check if Providers are enabled in device.
     * @return true if one of the Providers is enabled in device.
     */
    @Override
    public boolean isLocationEnabled() {
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (gps_enabled || network_enabled) {
            return true;
        }
        return false;
    }

    /**
     * Called when user location is changed.
     * EventBus broadcast location data and is observed in MainPresenter by onGPSDeviceEvent(Location location)
     * @param location current location of the user
     */
    @Override
    public void onLocationChanged(Location location) {
        if (location.getLatitude() != 0 && location.getLongitude() != 0) {
            Log.e(locationListenerTag, location.getLatitude() + "\n" + location.getLongitude());
            // Event that passes the Latitude and Longitude values to presenter and make network call
            EventBus.getDefault().post(location);
            // Removes the Location Request Updates
            locationManager.removeUpdates(this);
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
