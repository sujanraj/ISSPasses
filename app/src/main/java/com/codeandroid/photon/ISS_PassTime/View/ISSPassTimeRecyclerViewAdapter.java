package com.codeandroid.photon.ISS_PassTime.View;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codeandroid.photon.ISS_PassTime.Model.Retrofit.OpenNotifyPOJO.Response;
import com.codeandroid.photon.ISS_PassTime.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * ISSPassTimeRecyclerViewAdapter is the RecyclerViewAdapter.
 * It will set the Layout file for the View in RecyclerView.
 * Gives the values to views from the data received from Network response.
 */
public class ISSPassTimeRecyclerViewAdapter extends RecyclerView.Adapter<ISSPassTimeDataViewHolder> {

    private final List<Response> responseList;

    // Getting the Data from the Network Response.
    public ISSPassTimeRecyclerViewAdapter(List<Response> responseList) {

        this.responseList = responseList;
    }

    // Inflating the Layout for the RecyclerView
    @Override
    public ISSPassTimeDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.datalayout, parent, false);
        return new ISSPassTimeDataViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ISSPassTimeDataViewHolder holder, int position) {
        if (position == 0) {
            holder.time.setText("Date and Time");
            holder.duration.setText("Duration (secs)");

        } else {
            position -= 1;
            Log.e("###", "size " + responseList.get(position).getDuration() + "/n" + responseList.get(position).getRisetime() +
                    new Date(responseList.get(position).getRisetime()));
            holder.time.setText(getFormattedDate(responseList.get(position).getRisetime()));
            holder.duration.setText(responseList.get(position).getDuration().toString());


        }

    }

    /**
     * Change the date format
     * @param format integer date
     * @return yyyy-MM-DD HH:MM:SS.SSS date format
     */
    private String getFormattedDate(Integer format) {
        Date date = new Date(format * 1000L);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss.SSS");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Log.e("time", dateFormat.format(date));
        return dateFormat.format(date);
    }

    // Passing the Required number of views for the RecyclerView
    @Override
    public int getItemCount() {
        return responseList.size() + 1;
    }
}
